Requirements
=========

In order to collaborate on this project, please clone this repository.

.. code-block::

   $ git clone https://gitlab.com/szangyi/dev-env-mandatory-2.git


Once the project is cloned to your machine locally, open it in your editor, e.g.

.. code-block::

   $ cd djangobank && code .


To run the project locally, ensure you have Docker installed on your computer. If you are not familiar with Docker you can read their documentation here:  https://docs.docker.com/installation/ .

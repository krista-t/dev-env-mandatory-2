.. Bank App in Docker documentation master file, created by
   sphinx-quickstart on Sun Dec 18 10:35:37 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Bank App in Docker's documentation!
==============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   1-about
   2-the-project
   3-requirements
   4-workflow
   5-sqa




from django.test import TestCase
from django.contrib.auth.models import User
from account_management_app. models import Account, Customer
from . models import Employee
from django.urls import reverse


class EmployeeTestCase(TestCase):
    def test_login_view(self):
        response = self.client.get(path='/accounts/login/')
        assert response.status_code == 200
        assert True

    def setUp(self):
        self.admin_credentials = {
            'username': 'employee',
            'password': 'test123'
        }
        self.credentials = {
            'username': 'user',
            'password': 'test123'
        }

        self.admin = User.objects.create_user(**self.admin_credentials)
        self.admin_id = self.admin.pk
        self.user = User.objects.create_user(**self.credentials)

        default_employee = Customer(user=self.admin, phone_number='555666', rank='silver')
        default_employee.save()

        self.customer = Customer(user=self.user, phone_number='555666', rank='silver')
        self.customer.save()
        self.user_id = self.customer.pk

        self.account = Account.objects.create(user=self.user, name='Account', is_customer=True)
        self.account_id = self.account.pk
        self.account.save()

        default_employee = Employee(user=self.admin)

        self.client.post('/accounts/login/', self.admin_credentials, follow=True)

    def test_employee_login(self):
        response = self.client.post('/accounts/login/', self.credentials, follow=True)
        assert response.status_code == 200
        self.assertEqual(int(self.client.session['_auth_user_id']), self.admin_id)

    def test_all_customers_count(self):
        customers = Customer.objects.all()
        customer_count = customers.count()
        assert customer_count == 2

    def test_get_all_customers(self):
        customers = Customer.objects.all()
        customer_count = customers.count()
        assert customer_count == 2

    def test_get_all_customers_view(self):
        customers = Customer.objects.all()
        context = {
            'customers': customers
        }
        response = self.client.get(reverse('employee_app:all_customers'), context)
        assert response.status_code == 200

    def test_get_account_transactions(self):
        account = Account.objects.get(account_number=self.account_id)
        balance = account.balance
        assert balance == 0

    def test_customer_rank(self):
        print(self.user_id)
        customer = Customer.objects.get(pk=self.user_id)
        print(customer)
        customer_rank = customer.rank
        print(customer_rank)
        assert customer_rank == 'silver'

    def test_customer_accounts(self):
        customer = Customer.objects.get(pk=self.user_id)
        accounts = Account.objects.filter(user=customer.user)
        accounts_count = accounts.count()
        assert accounts_count == 1

    def test_load_index(self):
        self.client.login(username='employee', password='test123')
        response = self.client.get('/bank/all-customers', follow=True)
        self.assertTemplateUsed(response, 'login_app/login.html')
        assert response.status_code == 200

    def test_load_login(self):
        self.client.login(username='employee', password='test123')
        response = self.client.get('/bank', follow=True)
        self.assertTemplateUsed(response, 'login_app/login.html')
        assert response.status_code == 200
